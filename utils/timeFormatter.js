function getLocalDateTime() {
  const date = new Date();
  let options = {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit"
  };
  return date.toLocaleTimeString("en-us", options);
}

function getLocalDate(index) {
  var date = new Date();
  date.setDate(new Date().getDate() + index);
  let options = {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric"
  };
  const dateArray = date
    .toLocaleTimeString("en-us", options)
    .split(",")
    .splice(1, 2);
  return dateArray.join(",").trim();
}

exports.getLocalDateTime = getLocalDateTime;
exports.getLocalDate = getLocalDate;
