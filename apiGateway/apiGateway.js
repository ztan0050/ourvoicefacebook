const axios = require("axios");
const { join } = require("path");
const ENV_FILE = join(__dirname, ".env");
require("dotenv").config({ path: ENV_FILE });

const APIGATEWAY_URL = process.env.AwsApiGatewayEndpoint;

async function getUser(userId) {
  try {
    return await axios({
      method: "GET",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/user/${userId}`,
    });
  } catch (error) {
    return error.response;
  }
}

async function verifyUser(email) {
  try {
    return await axios({
      method: "GET",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/user/whitelist`,
      params: { email: email },
    });
  } catch (error) {
    return error.response;
  }
}

async function postUser(user) {
  try {
    return await axios({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/user`,
      data: {
        userId: user.userId,
        regDate: user.regDate,
        email: user.email,
        name: user.name,
      },
    });
  } catch (error) {
    return error.response;
  }
}

async function postPost(post) {
  try {
    return await axios({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/post`,
      data: {
        postDate: post.postDate,
        category: post.category,
        postedBy: post.postedBy,
        content: post.content,
        comments: [],
        moderations: [],
      },
    });
  } catch (error) {
    return error.response;
  }
}

async function getPost(postId) {
  try {
    return await axios({
      method: "GET",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/post/${postId}`,
    });
  } catch (error) {
    return error.response;
  }
}

async function postComment(postId, comment) {
  try {
    return await axios({
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/post/${postId}`,
      data: {
        commId: comment.commId,
        commDate: comment.commDate,
        commBy: comment.commBy,
        commContent: comment.commContent,
      },
      params: {
        updateType: "postComment",
      },
    });
  } catch (error) {
    return error.response;
  }
}

async function updateFbId(postId, facebookId) {
  try {
    return await axios({
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/post/${postId}`,
      data: { facebookId: facebookId },
      params: {
        updateType: "updateFacebookId",
      },
    });
  } catch (error) {
    return error.response;
  }
}

async function getReviews() {
  try {
    return await axios({
      method: "GET",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/review`,
    });
  } catch (error) {
    return error.response;
  }
}

async function postReview(postId, moderation, commId, commIndex) {
  try {
    return await axios({
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/review`,
      data: {
        moderator: moderation.moderator,
        moderDate: moderation.moderDate,
        content: moderation.content,
      },
      params: { postId: postId, commId: commId, commIndex: commIndex },
    });
  } catch (error) {
    return error.response;
  }
}

async function deleteReview(postId, commId) {
  try {
    return await axios({
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      url: `${APIGATEWAY_URL}/review`,
      params: { postId: postId, commId: commId },
    });
  } catch (error) {
    return error.response;
  }
}

exports.getUser = getUser;
exports.verifyUser = verifyUser;
exports.postUser = postUser;
exports.postPost = postPost;
exports.getPost = getPost;
exports.postComment = postComment;
exports.updateFbId = updateFbId;
exports.getReviews = getReviews;
exports.postReview = postReview;
exports.deleteReview = deleteReview;
