const { getLocalDateTime } = require("../utils/timeFormatter");

function generateSummaryCard(message, labelList) {
  const currentDateTime = getLocalDateTime();
  return {
    $schema: "http://adaptivecards.io/schemas/adaptive-card.json",
    type: "AdaptiveCard",
    version: "1.0",
    body: [
      {
        type: "Container",
        items: [
          {
            type: "TextBlock",
            text: "Anonymous Post Summary",
            weight: "bolder",
            size: "medium",
          },
          {
            type: "ColumnSet",
            columns: [
              {
                type: "Column",
                width: "auto",
                items: [
                  {
                    type: "Image",
                    url:
                      "https://firebasestorage.googleapis.com/v0/b/wechaty-demo.appspot.com/o/Images%2FAnonymous.png?alt=media&token=41df1eee-e1a4-4e4e-9551-a143d54ad319",
                    size: "small",
                    style: "person",
                  },
                ],
              },
              {
                type: "Column",
                width: "stretch",
                items: [
                  {
                    type: "TextBlock",
                    text: "Anonymity Bot",
                    weight: "bolder",
                    wrap: true,
                  },
                  {
                    type: "TextBlock",
                    spacing: "none",
                    text: `Created at: ${currentDateTime}`,
                    isSubtle: true,
                    wrap: true,
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        type: "Container",
        items: [
          {
            type: "TextBlock",
            text: message,
            wrap: true,
          },
          {
            type: "FactSet",
            facts: [
              {
                title: "Facebook Page:",
                value: "AnonymityDemo",
              },
              {
                title: "Categories:",
                value: labelList.join(", "),
              },
            ],
          },
        ],
      },
    ],
  };
}

exports.generateSummaryCard = generateSummaryCard;
