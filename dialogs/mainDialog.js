const {
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { getUser } = require("../apiGateway/apiGateway");
const { RegDialog } = require("../dialogs/regDialog");
const { UserDialog } = require("../dialogs/userDialog");
const { AdminDialog } = require("../dialogs/adminDialog");

const WATERFALL_DIALOG = "WATERFALL_DIALOG";

class MainDialog extends ComponentDialog {
  constructor(userState) {
    super("MAIN_DIALOG");
    this.userState = userState;

    this.addDialog(new RegDialog());
    this.addDialog(new UserDialog());
    this.addDialog(new AdminDialog());

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.finalStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes
   * it through the dialog system. If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  // Initial step for user identification.
  async initialStep(stepContext) {
    const userId = stepContext.context.activity.from.id;
    // const userId = "a5b08319-2780-4fcd-a789-8b68c2ab9881";
    const getUserRes = await getUser(userId);

    if (getUserRes.status !== 200) {
      return await stepContext.beginDialog("RegDialog", {});
    } else {
      if (getUserRes.data.isAdmin)
        return await stepContext.beginDialog("AdminDialog", {});
      else return await stepContext.beginDialog("UserDialog", {});
    }
  }

  // Final step to end the conversation.
  async finalStep(stepContext) {
    return stepContext.endDialog();
  }
}

module.exports.MainDialog = MainDialog;
