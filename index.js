// Import required bot services.
const {
  BotFrameworkAdapter,
  ConversationState,
  InputHints,
  UserState,
} = require("botbuilder");

// Import required packages.
const { join } = require("path");
// const cron = require("node-cron");
const bodyParser = require("body-parser");
const xhub = require("express-x-hub");
const cron = require("node-cron");
const { createServer, plugins } = require("restify");
const { generateName } = require("./utils/generateName");
const MIN_REVIEW = 1;

// Retrieve the .env file under the current path.
const ENV_FILE = join(__dirname, ".env");
// Set the process.env variables with the dotenv package.
require("dotenv").config({ path: ENV_FILE });

// Configure the connection settings to connect to Cosmos DB.
const { CosmosDbStorage } = require("botbuilder-azure");
const cosmosStorage = new CosmosDbStorage({
  serviceEndpoint:
    process.env.COSMOS_DB_SERVICE_ENDPOINT,
  authKey: process.env.CosmosAuthKey,
  databaseId: process.env.COSMOS_DATABASE,
  collectionId: process.env.CONTAINER,
});

// Define the bot's main dialog.
const { DialogAndWelcomeBot } = require("./bots/dialogAndWelcomeBot");
const { MainDialog } = require("./dialogs/mainDialog");
const {
  getReviews,
  getPost,
  updateFbId,
  deleteReview,
} = require("./apiGateway/apiGateway");

const graph = require("fbgraph");
const PAGE_TOKEN = process.env.FacebookPageToken;
graph.setAccessToken(PAGE_TOKEN);

// Create bot framework adapter.
// Use the transcript logger middleware to log all incoming and outgoing messages.
const adapter = new BotFrameworkAdapter({
  appId: process.env.MicrosoftAppId,
  appPassword: process.env.MicrosoftAppPassword,
});

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
  // This check writes out errors to console log.
  console.error(`\n [onTurnError]: ${error}`);

  // Send error message to the user.
  const onTurnErrorMessage = error;

  await context.sendActivity(
    onTurnErrorMessage,
    onTurnErrorMessage,
    InputHints.ExpectingInput
  );
  // Clear out state.
  await conversationState.delete(context);
};

// Define a state store for your bot.
// A bot requires a state store to persist the dialog and user state between messages.
let conversationState, userState;

// Using Cosmos DB for storing conversation and user state.
conversationState = new ConversationState(cosmosStorage);
userState = new UserState(cosmosStorage);

// Create HTTP server.
const server = createServer();
server.listen(process.env.port || process.env.PORT || 3980, function () {
  console.log(`\n${server.name} listening to ${server.url}`);
});
server.use(plugins.queryParser());

// Create the main dialog.
const mainDialog = new MainDialog(userState);
const bot = new DialogAndWelcomeBot(conversationState, userState, mainDialog);

// Listen for incoming activities and route them to the bot registration dialog.
server.post("/api/messages", (req, res) => {
  // Route received a request to adapter for processing.
  adapter.processActivity(req, res, async (turnContext) => {
    if (req.body.type === "event") {
      console.log("/api/messages: ", req.body.value);
      return res.status(200);
    }
    return await bot.run(turnContext);
  });
});

// Add scheduler for making the posts and comments.
cron.schedule("0 0 9 * * *", async () => {
  console.log("Scheduled Posting...");
  const getReviewRes = await getReviews();
  if (getReviewRes.status !== 200)
    return console.error("Error: ", getReviewRes.data);

  // Check the number of moderations for the current post.
  // If reachs minimum moderation requirement, then publish the post to Facebook.
  const reviews = getReviewRes.data;
  for (let i = 0; i < reviews.length; i++) {
    const item = reviews[i];
    if (item.reviewNo >= MIN_REVIEW && item.commId === "null") {
      const getPostRes = await getPost(item.postId);
      if (getPostRes.status !== 200) console.error("Error: ", getPostRes.data);

      // Create a new pure text post POST /page-id/feed.
      const postContent = getPostRes.data;
      const name = generateName();
      const message = `${
        postContent.postId
        }\n\n Posted by ${name}: ${postContent.moderations[
          postContent.moderations.length - 1
        ].content.join("\r\n\n")}\n\n${postContent.category
          .map((c) => `#OurVoice_${c.replace(" ", "_")}`)
          .join(" ")}`;

      // console.log("Content Posted: ", message);
      // const updateFbRes = await updateFbId(
      //   postContent.postId,
      //   "101349184773850"
      // );
      // if (updateFbRes.status !== 200) console.error(updateFbRes.data);
      // const deleteRes = await deleteReview(postContent.postId);
      // if (deleteRes.status !== 200) console.error(deleteRes.data);

      graph.post(
        "/101349184773850/feed", // Live Facebook Page
        { message: message },
        async function (err, res) {
          if (err) console.error(err);
          console.log("Post feed response: ", res);
          const updateFbRes = await updateFbId(postContent.postId, res.id);
          if (updateFbRes.status !== 200) console.error(updateFbRes.data);
          const deleteRes = await deleteReview(postContent.postId);
          if (deleteRes.status !== 200) console.error(deleteRes.data);
        }
      );
    }
  }
});

server.get("/facebook", function (req, res) {
  console.log(req.query);
  if (
    req.query["hub.mode"] == "subscribe" &&
    req.query["hub.verify_token"] == "token"
  ) {
    res.sendRaw(req.query["hub.challenge"]);
  } else {
    res.status(400);
  }
});

server.post(
  "/facebook",
  xhub({
    algorithm: "sha1",
    secret: process.env.FacebookXhubSecret,
  }),
  bodyParser.json(),
  function (req, res) {
    console.log("Facebook request body:", req.body);
    console.log("Facebook request changed fields: ", req.body.entry[0].changes);

    if (!req.isXHubValid()) {
      console.log(
        "Warning - request header X-Hub-Signature not present or invalid"
      );
      return res.status(401);
    }

    res.status(200);
  }
);
